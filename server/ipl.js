function matchesPlayedByPerYear(delivery, match) {
    let matchPerYear = {};
    match.forEach(obj => {
        if (matchPerYear[obj.season]) {
            matchPerYear[obj.season] += 1;
        }
        else {
            matchPerYear[obj.season] = 1;
        }

    })
    return matchPerYear;

}
function matchesWonPerTeamPerYear(delivery, match) {
    machesWonByTeam = {}

    match.forEach(obj => {

        if (machesWonByTeam.hasOwnProperty(obj.winner)) {
            //console.log(obj.winner);
            if (machesWonByTeam[obj.winner][obj.season]) {
                machesWonByTeam[obj.winner][obj.season] += 1;
            }
            else {
                machesWonByTeam[obj.winner][obj.season] = 1;
            }
        }
        else {
            machesWonByTeam[obj.winner] = {};
            machesWonByTeam[obj.winner][obj.season] = 1;
        }
    })
    return machesWonByTeam;

}
function extraRunsConcededPerTeam(delivery, match) {
    let match2016 = [];
    let delivery2016 = [];
    let extraRunConcededTeam = {};
    match2016 = match.filter((obj) => {
        return obj.season === '2016'
    }).map(obj => obj.id)
    delivery2016 = delivery.filter(obj => {
        return match2016.includes(obj.match_id);
    }).map(obj => obj)


    delivery2016.forEach(deliveryObj => {
        if (extraRunConcededTeam.hasOwnProperty(deliveryObj.bowling_team)) {
            extraRunConcededTeam[deliveryObj.bowling_team] += parseInt(deliveryObj.extra_runs)
        }
        else {
            extraRunConcededTeam[deliveryObj.bowling_team] = parseInt(deliveryObj.extra_runs)
        }

    })




    return extraRunConcededTeam;


}
function top10EconomicalBowlers2015(delivery, match) {
    let match2015 = [];
    let EconomicalBowlers2015 = {};

    match2015 = match.filter((obj) => {
        return obj.season === '2015'
    })

    match2015.forEach(matchObj => {
        delivery.forEach(deliveryObj => {
            //console.log(matchObj.id +" "+ deliveryObj.match_id)
            if (matchObj.id === deliveryObj.match_id) {
                if (EconomicalBowlers2015.hasOwnProperty(deliveryObj.bowler)) {

                    EconomicalBowlers2015[deliveryObj.bowler].economi += parseInt(deliveryObj.total_runs);
                    EconomicalBowlers2015[deliveryObj.bowler].deli += 1;
                }
                else {

                    EconomicalBowlers2015[deliveryObj.bowler] = {};
                    EconomicalBowlers2015[deliveryObj.bowler].economi = +deliveryObj.total_runs;
                    EconomicalBowlers2015[deliveryObj.bowler].deli = 1;
                }
            }
        })

    })
    let top10Economi = [];
    for (let key in EconomicalBowlers2015) {
        // console.log(EconomicalBowlers2015[key].deli);
        let over;
        over = EconomicalBowlers2015[key].deli / 6;
        EconomicalBowlers2015[key].economi /= over;
        top10Economi.push([key, EconomicalBowlers2015[key].economi])


    }
    top10Economi.sort(function (a, b) {
        return a[1] - b[1];
    })

    top10Economi = top10Economi.slice(0, 10);

    return top10Economi;
}

module.exports = {
    matchesPlayedByPerYear, matchesWonPerTeamPerYear,
    extraRunsConcededPerTeam, top10EconomicalBowlers2015
};
